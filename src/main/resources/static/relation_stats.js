const relationData = [
  {
    "relation": "Cites",
    "count": 2261807134
  },
  {
    "relation": "Compiles",
    "count": 5585
  },
  {
    "relation": "Continues",
    "count": 372253
  },
  {
    "relation": "Describes",
    "count": 1824
  },
  {
    "relation": "Documents",
    "count": 357118
  },
  {
    "relation": "HasAmongTopNSimilarDocuments",
    "count": 51170215
  },
  {
    "relation": "HasPart",
    "count": 4183736
  },
  {
    "relation": "HasVersion",
    "count": 384695
  },
  {
    "relation": "IsAmongTopNSimilarDocuments",
    "count": 51170215
  },
  {
    "relation": "IsIdenticalTo",
    "count": 66826
  },
  {
    "relation": "IsMetadataFor",
    "count": 221
  },
  {
    "relation": "IsNewVersionOf",
    "count": 647755
  },
  {
    "relation": "IsRelatedTo",
    "count": 140425744
  },
  {
    "relation": "IsSupplementTo",
    "count": 2480789
  },
  {
    "relation": "Obsoletes",
    "count": 2707
  },
  {
    "relation": "References",
    "count": 9433114
  },
  {
    "relation": "Requires",
    "count": 708
  },
  {
    "relation": "Reviews",
    "count": 38378
  }
]
