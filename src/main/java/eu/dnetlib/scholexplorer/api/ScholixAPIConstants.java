package eu.dnetlib.scholexplorer.api;

public class ScholixAPIConstants {

    public static final String API_V1_NAME = "Scholexplorer API V1.0";
    public static final String API_V2_NAME = "Scholexplorer API V2.0";
    public static final String API_V3_NAME = "Scholexplorer API V3.0";

    public static String API_DESCRIPTION =" <p style=\"text-align:center;\"><img src=\"/logo.png\" alt=\"ScholeXplorer\"> </p>" +
            "TThe Scholexplorer API support the discovery of Scholix relationships filtering by the following parameters: source ID, target ID, semantics, publisher, entity type (publication, dataset, software, other), or publishing date. As of today, three API versions area available:" +
            "<ul><li><a href=\"/v1/ui\">version 1.0</a></li>" +
            "<li><a href=\"/v2/ui\">version 2.0</a></li>" +
            "<li><a href=\"/v3/ui\">version 3.0</a></li>" +
            "<p>For more information, please visit the <a href=\"https://graph.openaire.eu/docs/apis/scholexplorer/api\">API documentation</a></p>";



    public static  String SCHOLIX_MANAGER_COUNTER_NAME= "scholixLinkCounter";
    public static final String SCHOLIX_MANAGER_TAG_NAME = "links";
    public static String SCHOLIX_COUNTER_PREFIX = "scholix";

}
