package eu.dnetlib.scholexplorer.api.model;

public class Summary {
    private String data;

    public String getData() {
        return data;
    }

    public void setBody(String data) {
        this.data = data;
    }
}
