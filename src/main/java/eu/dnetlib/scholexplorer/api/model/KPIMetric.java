package eu.dnetlib.scholexplorer.api.model;

import java.util.List;



public class KPIMetric {

    public static class ScholixRelMetric {
        public String sourceType;
        public String targetType;
        public long numberOfrelationships;

        public ScholixRelMetric(String sourceType, String targetType, long numberOfrelationships) {
            this.sourceType = sourceType;
            this.targetType = targetType;
            this.numberOfrelationships = numberOfrelationships;
        }
    }


    private Long numberOfRelationships;


    private List<ScholixRelMetric> relationMetrics;

    private Long numberOfEntity;

    public Long getNumberOfRelationships() {
        return numberOfRelationships;
    }

    public KPIMetric setNumberOfRelationships(Long numberOfRelationships) {
        this.numberOfRelationships = numberOfRelationships;
        return this;
    }

    public Long getNumberOfEntity() {
        return numberOfEntity;
    }

    public KPIMetric setNumberOfEntity(Long numberOfEntity) {
        this.numberOfEntity = numberOfEntity;
        return this;
    }

    public List<ScholixRelMetric> getRelationMetrics() {
        return relationMetrics;
    }

    public KPIMetric setRelationMetrics(List<ScholixRelMetric> relationMetrics) {
        this.relationMetrics = relationMetrics;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("scholexplorer_scholix ");
        sb.append(numberOfRelationships.toString());
        sb.append("\n");

        sb.append("scholexplorer_entities ");
        sb.append(numberOfEntity.toString());
        sb.append("\n");
        relationMetrics.forEach(r -> sb.append(String.format("scholexplorer_scholix_relations{source=\"%s\" target=\"%s\"} %d\n", r.sourceType, r.targetType,r.numberOfrelationships)));
        return sb.toString();
    }

}



