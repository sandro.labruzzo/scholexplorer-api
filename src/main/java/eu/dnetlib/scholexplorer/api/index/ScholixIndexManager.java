package eu.dnetlib.scholexplorer.api.index;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import eu.dnetlib.dhp.schema.sx.scholix.ScholixEntityId;
import eu.dnetlib.dhp.schema.sx.scholix.ScholixRelationship;
import eu.dnetlib.dhp.schema.sx.scholix.ScholixResource;
import eu.dnetlib.dhp.schema.sx.scholix.flat.ScholixFlat;
import eu.dnetlib.scholexplorer.api.ScholixException;
import eu.dnetlib.scholexplorer.api.TaggedCounter;
import eu.dnetlib.scholexplorer.api.model.KPIMetric;
import eu.dnetlib.scholexplorer.api.model.Summary;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeValuesSourceBuilder;
import org.elasticsearch.search.aggregations.bucket.composite.ParsedComposite;
import org.elasticsearch.search.aggregations.bucket.composite.TermsValuesSourceBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.stereotype.Component;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.elasticsearch.index.query.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ScholixIndexManager {
    @Autowired
    ElasticSearchProperties elasticSearchProperties;

    /**
     * The Elasticsearch template.
     */
    @Autowired
    ElasticSearchPool connectionPool;

    final ObjectMapper mapper = new ObjectMapper();

    /**
     * The enum Pid type prefix.
     */
    public enum RelationPrefix {
        /**
         * Source pid type prefix.
         */
        source,
        /**
         * Target pid type prefix.
         */
        target
    }

    @Autowired
    TaggedCounter myCounter;


    private List<String> extractIdentifiersFromScholix(SearchHits<ScholixFlat> scholix) {
        return scholix.stream()
                .flatMap(s ->
                        Stream.of(
                                s.getContent().getSourceId(),
                                s.getContent().getTargetId()))
                .distinct()
                .toList();
    }


    private Map<String, ScholixResource> retrieveResources(ElasticsearchRestTemplate client, List<String> ids) {
        final IdsQueryBuilder qb = new IdsQueryBuilder().addIds(ids.toArray(String[]::new));


        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(qb)
                .withPageable(PageRequest.of(0, ids.size()))
                .build();


        SearchHits<Summary> result = client.search(searchQuery, Summary.class, IndexCoordinates.of(elasticSearchProperties.getIndexResourceName()));
        return result.stream().map(r -> {
            try {
                return mapper.readValue(r.getContent().getData(), ScholixResource.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toMap(
                ScholixResource::getDnetIdentifier,
                v -> v,
                (a, b) -> a
        ));

    }

    private Scholix generateScholix(ScholixFlat flat, ScholixResource source, ScholixResource target) throws ScholixException {
        if (flat == null || source == null || target == null)
            throw new ScholixException("Error generating scholix null input");

        final Scholix scholix = new Scholix();
        scholix.setSource(source);
        scholix.setTarget(target);
        scholix.setIdentifier(flat.getIdentifier());
        final ScholixRelationship r = new ScholixRelationship();
        r.setSchema("datacite");
        r.setName(flat.getRelationType().toLowerCase());
        scholix.setRelationship(r);
        scholix.setPublicationDate(flat.getPublicationDate());
        scholix.setLinkprovider(flat.getLinkProviders().stream().map(p -> {
            final ScholixEntityId eid = new ScholixEntityId();
            eid.setName(p);
            return eid;
        }).toList());

        final Map<String, ScholixEntityId> publishers = new HashMap<>();
        if (source.getPublisher() != null)
            source.getPublisher().forEach(p -> publishers.put(p.getName(), p));
        if (target.getPublisher() != null)
            target.getPublisher().forEach(p -> publishers.put(p.getName(), p));

        scholix.setPublisher(publishers.values().stream().toList());
        return scholix;
    }

    private QueryBuilder createFinalQuery(final List<QueryBuilder> queries) throws ScholixException {

        if (queries == null || queries.isEmpty())
            throw new ScholixException("the list of queries must be not empty");


        if (queries.size() == 1) {
            return queries.get(0);
        } else {
            final BoolQueryBuilder b = new BoolQueryBuilder();
            b.must().addAll(queries);

            return b;
        }

    }

    public List<Pair<String, Long>> totalLinksByProvider(final String filterName) throws ScholixException {


        final QueryBuilder query = StringUtils.isNoneBlank(filterName) ? QueryBuilders.termQuery("linkProviders", filterName) : QueryBuilders.matchAllQuery();

        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withSearchType(SearchType.DEFAULT)
                .withPageable(PageRequest.of(0, 10))
                .addAggregation(
                        AggregationBuilders.terms("genres").field("linkProviders").size(100)
                                .minDocCount(1))
                .build();


        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();
            final SearchHits<ScholixFlat> hits = client.search(searchQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));

            final Aggregations aggregations = hits.getAggregations();
            if (aggregations == null)
                return null;

            return ((ParsedStringTerms) aggregations.get("genres")).getBuckets().stream().map(b -> new ImmutablePair<>(b.getKeyAsString(), b.getDocCount())).collect(Collectors.toList());
        } catch (ScholixException e) {
            throw e;
        } finally {
            if (connectionPool != null) {
                connectionPool.returnResource(resource);
            }
        }


    }


    private QueryBuilder createLinkPublisherQuery(final RelationPrefix prefix, final String publisher) throws ScholixException {
        if (prefix == null) {
            throw new ScholixException("prefix cannot be null");
        }
        return new NestedQueryBuilder(String.format("%s.publisher", prefix), new TermQueryBuilder(String.format("%s.publisher.name", prefix), publisher), ScoreMode.None);
    }


    public KPIMetric getKPI()  throws  ScholixException{


        List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();

        sources.add(new TermsValuesSourceBuilder("relationTypeSource")
                .field("sourceType"));
        sources.add(new TermsValuesSourceBuilder("relationTypeTarget")
                .field("targetType"));
        CompositeAggregationBuilder compositeAggregationBuilder = new CompositeAggregationBuilder(
                "Composite_relationTypes", sources)
                .size(10000);

        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withSearchType(SearchType.DEFAULT)
                .withPageable(PageRequest.of(0, 10))
                .addAggregation(
                        compositeAggregationBuilder)
                .build();

        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();



            final long total = client.count(new NativeSearchQueryBuilder().withSearchType(SearchType.DEFAULT).build(), ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));
            final long totalItem = client.count(new NativeSearchQueryBuilder().withSearchType(SearchType.DEFAULT).build(), ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexResourceName()));



            final KPIMetric metric = new KPIMetric();

            metric.setNumberOfRelationships(total);
            metric.setNumberOfEntity(totalItem);

            final SearchHits<ScholixFlat> hits = client.search(searchQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));

            final Aggregations aggregations = hits.getAggregations();
            if (aggregations == null)
                return null;
            metric.setRelationMetrics(
            ((ParsedComposite) aggregations.get("Composite_relationTypes"))
                    .getBuckets()
                    .stream()
                    .map(b -> new KPIMetric.ScholixRelMetric(
                                b.getKey().get("relationTypeSource").toString(),
                                b.getKey().get("relationTypeTarget").toString(),
                            b.getDocCount())).toList());
            return metric;
        } catch (ScholixException e) {
            throw e;
        } finally {
            if (connectionPool != null) {
                connectionPool.returnResource(resource);
            }
        }
    }

    public List<Pair<String, Long>> totalLinksPublisher(final RelationPrefix prefix, final String filterName) throws ScholixException {


        final QueryBuilder query = StringUtils.isNoneBlank(filterName) ? createLinkPublisherQuery(prefix, filterName) : QueryBuilders.matchAllQuery();

        final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(query)
                .withSearchType(SearchType.DEFAULT)
                .withPageable(PageRequest.of(0, 10))
                .addAggregation(
                        AggregationBuilders.terms("publishers").field(String.format("%sPublisher", prefix.toString())).size(100)
                                .minDocCount(1))
                .build();


        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();
            final SearchHits<ScholixFlat> hits = client.search(searchQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));

            final Aggregations aggregations = hits.getAggregations();
            if (aggregations == null)
                return null;

            return ((ParsedStringTerms) aggregations.get("publishers")).getBuckets().stream().map(b -> new ImmutablePair<>(b.getKeyAsString(), b.getDocCount())).collect(Collectors.toList());
        } catch (ScholixException e) {
            throw e;
        } finally {
            if (connectionPool != null) {
                connectionPool.returnResource(resource);
            }
        }
    }

    private void incrementPidCounter(RelationPrefix prefix, String value) {
        switch (value.toLowerCase()) {
            case "doi": {
                myCounter.increment(String.format("%s_doi", prefix));
                break;
            }
            case "pmc": {
                myCounter.increment(String.format("%s_pmc", prefix));
                break;
            }
            default:
                myCounter.increment(String.format("%s_other", prefix));
        }
    }


    /**
     * Links related to item with OpenAIRE Identifier
     * @param sourceId the source openAIRE Identifier
     * @param page the current page
     * @return a Pair of total number of elements and the current page
     */
    public Pair<Long, List<Scholix>> linksToOpenAIREId(final String sourceId, final Integer page) throws ScholixException {
        final List<QueryBuilder> queries = new ArrayList<>();
        queries.add(QueryBuilders.termQuery("sourceId", sourceId));

        QueryBuilder result = createFinalQuery(queries);

        NativeSearchQuery finalQuery = new NativeSearchQueryBuilder()
                .withQuery(result)
                .withPageable(PageRequest.of(page, 100))
                .build();


        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();


            long tt = client.count(finalQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));

            SearchHits<ScholixFlat> scholixRes = client.search(finalQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));


            if (tt > 0) {
                final Map<String, ScholixResource> idMap = retrieveResources(client, extractIdentifiersFromScholix(scholixRes));

                return new ImmutablePair<>(tt, scholixRes.stream().map(SearchHit::getContent).map(s -> {
                    try {
                        return generateScholix(s, idMap.get(s.getSourceId()), idMap.get(s.getTargetId()));
                    } catch (ScholixException e) {
                        throw new RuntimeException(e);
                    }
                }).toList());
            } else return new ImmutablePair<>(tt, new ArrayList<>());
        } catch (ScholixException e) {
            throw e;
        } finally {
            if (connectionPool != null) {
                connectionPool.returnResource(resource);
            }

        }

    }


    /**
     * Links from pid pair.
     *
     * @param linkProvider    the link provider
     * @param targetPid       the target pid
     * @param targetPidType   the target pid type
     * @param targetPublisher the target publisher
     * @param targetType      the target type
     * @param sourcePid       the source pid
     * @param sourcePidType   the source pid type
     * @param sourcePublisher the source publisher
     * @param sourceType      the source type
     * @param page            the page
     * @return the pair
     * @throws ScholixException the scholix exception
     */
    public Pair<Long, List<Scholix>> linksFromPid(final String linkProvider,
                                                  final String targetPid, final String targetPidType, final String targetPublisher,
                                                  final String targetType, final String sourcePid, final String sourcePidType,
                                                  final String sourcePublisher,
                                                  final String sourceType,
                                                  final String relation,
                                                  final String fromDate,
                                                  final String toDate,
                                                  final Integer page) throws ScholixException {


        if (sourcePid == null && sourcePidType == null && sourceType == null && targetType == null && targetPid == null && targetPidType == null && sourcePublisher == null && targetPublisher == null && linkProvider == null)
            throw new ScholixException("One of sourcePid, targetPid, sourcePublisher, targetPublisher, linkProvider should be not null");

        final List<QueryBuilder> queries = new ArrayList<>();

        if (StringUtils.isNoneBlank(linkProvider)) {
            myCounter.increment("linkProvider");
            queries.add(QueryBuilders.matchQuery("linkProviders", linkProvider)
            );
        }

        if (StringUtils.isNoneBlank(targetPid)) {
            myCounter.increment("targetPid");
            final BoolQueryBuilder b = new BoolQueryBuilder();
            b.should().addAll(List.of(QueryBuilders.termQuery("targetPid", targetPid), QueryBuilders.termQuery("targetId", targetPid)));
            queries.add(b);
        }
        if (StringUtils.isNoneBlank(sourcePid)) {
            myCounter.increment("sourcePid");

            final BoolQueryBuilder b = new BoolQueryBuilder();
            b.should().addAll(List.of(QueryBuilders.termQuery("sourcePid", sourcePid), QueryBuilders.termQuery("sourceId", sourcePid)));
            queries.add(b);
//            queries.add(QueryBuilders.termQuery("sourceId", sourcePid));

        }

        if (StringUtils.isNoneBlank(targetPidType)) {
            assert targetPidType != null;
            incrementPidCounter(RelationPrefix.target, targetPidType);
            queries.add(QueryBuilders.termQuery("targetPidType", targetPidType));
        }
        if (StringUtils.isNoneBlank(sourcePidType)) {
            assert sourcePidType != null;
            incrementPidCounter(RelationPrefix.source, sourcePidType);
            queries.add(QueryBuilders.termQuery("sourcePidType", sourcePidType));
        }

        if (StringUtils.isNoneBlank(targetType)) {
            myCounter.increment(String.format("targetType_%s", targetType));
            queries.add(QueryBuilders.termQuery("targetType", targetType));
        }

        if(StringUtils.isNotBlank(fromDate)){
            queries.add(QueryBuilders.rangeQuery("publicationDate").gte(fromDate));
        }
        if(StringUtils.isNotBlank(toDate)){
            queries.add(QueryBuilders.rangeQuery("publicationDate").lte(toDate));
        }

        if (StringUtils.isNotBlank(sourceType)) {
            myCounter.increment(String.format("sourceType_%s", sourceType));
            queries.add(QueryBuilders.termQuery("sourceType", sourceType));
        }

        if (StringUtils.isNotBlank(targetPublisher)) {
            myCounter.increment("targetPublisher");
            queries.add(QueryBuilders.matchQuery("targetPublisher", targetPublisher));
        }

        if (StringUtils.isNotBlank(sourcePublisher)) {
            myCounter.increment("sourcePublisher");
            queries.add(QueryBuilders.matchQuery("sourcePublisher", sourcePublisher));
        }

        if (StringUtils.isNoneBlank(relation)) {
            myCounter.increment("relationType");
            queries.add(QueryBuilders.termQuery("relationType", relation));
        }
        QueryBuilder result = createFinalQuery(queries);

        NativeSearchQuery finalQuery = new NativeSearchQueryBuilder()
                .withQuery(result)
                .withPageable(PageRequest.of(page, 100))
                .build();


        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();


            long tt = client.count(finalQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));

            SearchHits<ScholixFlat> scholixRes = client.search(finalQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));


            if (tt > 0) {
                final Map<String, ScholixResource> idMap = retrieveResources(client, extractIdentifiersFromScholix(scholixRes));

                return new ImmutablePair<>(tt, scholixRes.stream().map(SearchHit::getContent).map(s -> {
                    try {
                        return generateScholix(s, idMap.get(s.getSourceId()), idMap.get(s.getTargetId()));
                    } catch (ScholixException e) {
                        throw new RuntimeException(e);
                    }
                }).toList());
            } else return new ImmutablePair<>(tt, new ArrayList<>());
        } catch (ScholixException e) {
            throw e;
        } finally {
            if (connectionPool != null) {
                connectionPool.returnResource(resource);
            }

        }
    }

    public List<Scholix> findPage(final String relType) throws ScholixException {
        Pair<RestHighLevelClient, ElasticsearchRestTemplate> resource = null;
        try {
            resource = connectionPool.getResource();
            ElasticsearchRestTemplate client = resource.getValue();
            final NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()

                    .withQuery(QueryBuilders.termQuery("relationType", relType))

                    .withSearchType(SearchType.DEFAULT)
                    .withPageable(PageRequest.of(0, 20))
                    .build();

            SearchHits<ScholixFlat> search = client.search(searchQuery, ScholixFlat.class, IndexCoordinates.of(elasticSearchProperties.getIndexName()));


            final Map<String, ScholixResource> idMap = retrieveResources(client, extractIdentifiersFromScholix(search));

            return search.stream().map(SearchHit::getContent).map(s -> {
                try {
                    return generateScholix(s, idMap.get(s.getSourceId()), idMap.get(s.getTargetId()));
                } catch (ScholixException e) {
                    throw new RuntimeException(e);
                }
            }).toList();
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        } finally {
            if (connectionPool != null && resource != null) {
                connectionPool.returnResource(resource);
            }
        }
        return null;
    }

}
