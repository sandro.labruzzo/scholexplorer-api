package eu.dnetlib.scholexplorer.api.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping({
            "/doc", "/swagger"
    })
    public String apiDoc() {
        return "redirect:swagger-ui/index.html";
    }

    @GetMapping({
            "/v1/ui"
    })
    public String v1Doc() {
        return "redirect:/swagger-ui/index.html?urls.primaryName=Scholexplorer%20API%20V1.0";
    }


    @GetMapping({
            "/v2/ui"
    })
    public String v2Doc() {
        return "redirect:/swagger-ui/index.html?urls.primaryName=Scholexplorer%20API%20V2.0";
    }

    @GetMapping({
            "/v3/ui","/"
    })
    public String v3Doc() {
        return "redirect:/swagger-ui/index.html?urls.primaryName=Scholexplorer%20API%20V3.0";
    }





}
