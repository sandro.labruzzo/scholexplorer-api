package eu.dnetlib.scholexplorer.api.controller;
import eu.dnetlib.dhp.schema.sx.api.model.v1.LinkPublisher;
import eu.dnetlib.scholexplorer.api.ScholixException;
import eu.dnetlib.scholexplorer.api.index.ScholixIndexManager;
import eu.dnetlib.scholexplorer.api.model.KPIMetric;
import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/kpi")
@Tag(name = "kpi")
public class KPIController {
    @Autowired
    private ScholixIndexManager manager;

//    @Operation(summary = "Get Scholix KPI", description = "returns the current KPI")
//    @GetMapping("/getKPI")
//    public String getKPI() throws ScholixException {
//
//        return manager.getKPI().toString();
//
//    }

    @RequestMapping(value="/getKPI", method=GET)
    public void foo(HttpServletResponse res) throws ScholixException {
        try {
            PrintWriter out = res.getWriter();
            out.println(manager.getKPI().toString());
            out.close();
        } catch (Exception ex) {
            throw new ScholixException(ex);
        }
    }

}
