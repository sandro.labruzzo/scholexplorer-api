package eu.dnetlib.scholexplorer.api.controller;

import eu.dnetlib.dhp.schema.sx.api.model.v2.PageResultType;
import eu.dnetlib.dhp.schema.sx.api.model.v2.ScholixType;
import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import eu.dnetlib.dhp.schema.sx.scholix.ScholixIdentifier;
import eu.dnetlib.scholexplorer.api.ScholixException;
import eu.dnetlib.scholexplorer.api.index.ScholixIndexManager;
import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v3")
@Tag(name = "Links : Operation related to the Scholix Links")
public class ScholixControllerV3 {

    @Autowired
    ScholixIndexManager repository;

    @Timed(value = "scholix_index_request_links", description = "Time taken to return links on Version 3.0 of Scholix")
    @Operation(summary = "Get Scholix Links")
    @GetMapping("/Links")
    public PageResultType links(
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a target pid") final String targetPid,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a target published in a Publisher named targetPublisher") final String targetPublisher,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a target type (publication, dataset, software, other)") final String targetType,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a source pid") final String sourcePid,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a source published in a Publisher named sourcePublisher") final String sourcePublisher,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix relationships having a source type (publication, dataset, software, other)") final String sourceType,
            @RequestParam(required = false)
            @Parameter(in = ParameterIn.QUERY, description = "Filter Scholix having a specific relationships ") final String relation,
             @Parameter(in = ParameterIn.QUERY,
             description = "Filter scholix Links after this date")
            @RequestParam(required = false)
            String from,
            @Parameter(in = ParameterIn.QUERY,
                    description = "Filter scholix Links until this date")
            @RequestParam(required = false)
            String to,
            @Parameter(in = ParameterIn.QUERY, description = "select page of result") final Integer page) throws Exception {

        if (StringUtils.isEmpty(sourcePid) && StringUtils.isEmpty(targetPid) && StringUtils.isEmpty(sourcePublisher) && StringUtils.isEmpty(targetPublisher) && StringUtils.isEmpty(sourceType)
                && StringUtils.isEmpty(targetType)) {
            throw new ScholixException(
                    "The method requires one of the following parameters: sourcePid, targetPid, sourcePublisher, targetPublisher, targetType, sourceType");
        }

        try {
            final int currentPage = page != null ? page : 0;
            final Pair<Long, List<Scholix>> scholixResult = repository
                    .linksFromPid(null, targetPid, null, targetPublisher, targetType, sourcePid, null, sourcePublisher, sourceType, relation, from, to, currentPage);
            final PageResultType pageResult = new PageResultType();
            pageResult.setTotalPages((scholixResult.getLeft().intValue() / 100) +1 );
            pageResult.setTotalLinks(scholixResult.getLeft().intValue());
            pageResult.setResult(scholixResult.getRight().stream()
                    .map(s ->  {
                        final String sourceDnetId = s.getSource().getDnetIdentifier();
                        final ScholixIdentifier sId = new ScholixIdentifier();
                        sId.setIdentifier(sourceDnetId);
                        sId.setSchema("openaireIdentifier");
                        s.getSource().getIdentifier().add(sId);
                        final String targetDnetId = s.getTarget().getDnetIdentifier();
                        final ScholixIdentifier tId = new ScholixIdentifier();
                        tId.setIdentifier(targetDnetId);
                        tId.setSchema("openaireIdentifier");
                        s.getTarget().getIdentifier().add(tId);
                        return s;
                    })
                    .map(ScholixType::fromScholix).collect(Collectors.toList()));
            pageResult.setCurrentPage(currentPage);
            return pageResult;
        } catch (final Throwable e) {
            throw new ScholixException("Error on requesting url ", e);
        }
    }
}
