package eu.dnetlib.scholexplorer.api;

import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.config.MeterFilter;
import io.micrometer.core.instrument.distribution.DistributionStatisticConfig;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.License;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.info.Info;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ScholexplorerApiApplication {

	@Value("${server.public_url}")
	private String serverPublicUrl;

	@Value("${server.public_desc}")
	private String serverPublicDesc;

	protected static final License AGPL_3_LICENSE =
			new License().name("GNU Affero General Public License v3.0 or later").url("https://www.gnu.org/licenses/agpl-3.0.txt");

	private final double scale = 1000000000;

	private final double[] histogramValues = new double[] {
			.005 * scale, .01 * scale, .25 * scale, .5 * scale, .75 * scale, scale, 2.5 * scale, 5.0 * scale, 7.5 * scale, 10.0 * scale
	};

	@Bean
	public TimedAspect timedAspect(final MeterRegistry meterRegistry) {
		final MeterFilter mf = new MeterFilter() {

			@Override
			public DistributionStatisticConfig configure(final Meter.Id id, final DistributionStatisticConfig config) {
				if (id.getName().startsWith(ScholixAPIConstants.SCHOLIX_COUNTER_PREFIX)) {

					return DistributionStatisticConfig.builder()
						.percentilesHistogram(false)
						.serviceLevelObjectives(histogramValues)
						.build()
						.merge(config);
				}
				return config;
			}
		};
		meterRegistry.config().meterFilter(mf);
		return new TimedAspect(meterRegistry);
	}





	@Bean
	public TaggedCounter myCounter(final MeterRegistry meterRegistry) {

		return new TaggedCounter(ScholixAPIConstants.SCHOLIX_MANAGER_COUNTER_NAME, ScholixAPIConstants.SCHOLIX_MANAGER_TAG_NAME, meterRegistry);
	}

	@Bean
	public GroupedOpenApi publicApiV1() {
		return GroupedOpenApi.builder()
				.group(ScholixAPIConstants.API_V1_NAME)
				.pathsToMatch("/v1/**")
				.build();
	}

	@Bean
	public GroupedOpenApi publicApiV2() {
		return GroupedOpenApi.builder()
				.group(ScholixAPIConstants.API_V2_NAME)
				.pathsToMatch("/v2/**")
				.build();
	}

	@Bean
	public GroupedOpenApi publicApiKPI() {
		return GroupedOpenApi.builder()
				.group(ScholixAPIConstants.API_V3_NAME)
				.pathsToMatch("/v3/**")
				.build();
	}


	@Bean
	public OpenAPI newSwaggerDocket() {
		final List<Server> servers = new ArrayList<>();
		if (StringUtils.isNotBlank(serverPublicUrl)) {
			final Server server = new Server();
			server.setUrl(serverPublicUrl);
			server.setDescription(serverPublicDesc);
			servers.add(server);
		}

		return new OpenAPI()
				.servers(servers)
				.info(getSwaggerInfo())
				.tags(new ArrayList<>());
	}

	private Info getSwaggerInfo() {
		return new Info()
				.title(swaggerTitle())
				.description(swaggerDesc())
				.license(AGPL_3_LICENSE);
	}

	protected String swaggerTitle() {
		return "ScholeExplorer APIs";
	}

	protected String swaggerDesc() {
		return ScholixAPIConstants.API_DESCRIPTION;
	}

	public static void main(String[] args) {
		SpringApplication.run(ScholexplorerApiApplication.class, args);
	}

}
