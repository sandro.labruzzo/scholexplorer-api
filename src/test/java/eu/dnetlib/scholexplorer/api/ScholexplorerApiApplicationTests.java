package eu.dnetlib.scholexplorer.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.dnetlib.dhp.schema.sx.scholix.Scholix;
import eu.dnetlib.scholexplorer.api.index.ScholixIndexManager;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;


class TestResult {
	public long totalTimeMs;
	public int totalItems;
	public String relationName;

	public long getTotalTimeMs() {
		return totalTimeMs;
	}

	public TestResult setTotalTimeMs(long totalTimeMs) {
		this.totalTimeMs = totalTimeMs;
		return this;
	}

	public int getTotalItems() {
		return totalItems;
	}

	public TestResult setTotalItems(int totalItems) {
		this.totalItems = totalItems;
		return this;
	}

	public String getRelationName() {
		return relationName;
	}

	public TestResult setRelationName(String relationName) {
		this.relationName = relationName;
		return this;
	}
}

@SpringBootTest
class ScholexplorerApiApplicationTests {

	@Autowired
	ScholixIndexManager scholixRepo;

	final ObjectMapper mapper = new ObjectMapper();


	final List<String> relations = Arrays.asList("IsDerivedFrom",
			"Continues",
			"References",
			"HasVersion",
			"IsVersionOf",
			"IsSupplementTo",
			"IsDocumentedBy",
			"Documents",
			"IsContinuedBy",
			"IsSupplementedBy",
			"IsSourceOf",
			"Reviews",
			"Cites",
			"IsAmongTopNSimilarDocuments",
			"IsPartOf",
			"HasPart",
			"IsIdenticalTo",
			"IsNewVersionOf",
			"IsRelatedTo",
			"HasAmongTopNSimilarDocuments",
			"IsCitedBy",
			"IsPreviousVersionOf",
			"IsReferencedBy",
			"IsVariantFormOf",
			"IsCompiledBy",
			"IsReviewedBy",
			"IsDescribedBy",
			"Compiles",
			"IsOriginalFormOf",
			"Describes");

	private List<TestResult> executeTest() throws Exception {
		Random rand = new Random();

		final List<TestResult> infos = new ArrayList<>();
		String currentRel1 =relations.get(rand.nextInt(relations.size()));
		long start = System.nanoTime();
		List<Scholix> result = scholixRepo.findPage(currentRel1);
		long total = (System.nanoTime() - start) /1000000;
		int cnt = result.size();

		infos.add(new TestResult().setRelationName(currentRel1).setTotalItems(cnt).setTotalTimeMs(total));

		String currentRel2 =relations.get(rand.nextInt(relations.size()));
		start = System.nanoTime();
		result = scholixRepo.findPage(currentRel2);
		long total2 = (System.nanoTime() - start) /1000000;
		int cnt2 = result.size();
		infos.add(new TestResult().setRelationName(currentRel2).setTotalItems(cnt2).setTotalTimeMs(total2));

		String currentRel3 =relations.get(rand.nextInt(relations.size()));
		start = System.nanoTime();
		result = scholixRepo.findPage(currentRel3);
		long total3 = (System.nanoTime() - start) /1000000;
		int cnt3 = result.size();
		infos.add(new TestResult().setRelationName(currentRel3).setTotalItems(cnt3).setTotalTimeMs(total3));
		return infos;
	}

	@Test
	void contextLoads() throws Exception {
		final List<TestResult> infos = new ArrayList<>();
		for (int i = 0; i < 200; i++) {
			infos.addAll(executeTest());
		}


		LongSummaryStatistics summary = infos.stream().map(TestResult::getTotalTimeMs).mapToLong(Long::longValue).summaryStatistics();

		System.out.println(summary.getMax());
		System.out.println(summary.getMin());
		System.out.println(summary.getAverage());
	}



	@Test
	void testlinksFromPid() throws ScholixException {
		Pair<Long, List<Scholix>> result = scholixRepo.linksFromPid(null, null, "doi", null, null, null, "pmid", null, null, null, null,null,0);
		result.getRight().forEach(
			s ->	Assertions.assertTrue(s.getTarget().getIdentifier().stream().anyMatch(p -> p.getSchema().equals("doi")))
		);

		result.getRight().forEach(
				s ->	Assertions.assertTrue(s.getSource().getIdentifier().stream().anyMatch(p -> p.getSchema().equals("pmid")))
		);


		result = scholixRepo.linksFromPid(null, null, null, null, "dataset", null, null, null, "publication","IsSupplementedBy", null,null,0);
		System.out.println(result.getLeft());
		result.getRight().forEach(
		 		s -> {
					 Assertions.assertEquals("dataset", s.getTarget().getObjectType());
					Assertions.assertEquals("publication", s.getSource().getObjectType());
					Assertions.assertEquals("issupplementedby", s.getRelationship().getName());

				});

		result = scholixRepo.linksFromPid(null, null, null, null, "publication", null, null, null, "publication","IsVersionOf", null,null,0);
		System.out.println(result.getLeft());
		result.getRight().forEach(
				s -> {
					Assertions.assertEquals("publication", s.getTarget().getObjectType());
					Assertions.assertEquals("publication", s.getSource().getObjectType());
					Assertions.assertEquals("IsVersionOf".toLowerCase(), s.getRelationship().getName());

				});



	}




}
