# Scholexplorer Api

![Alt text](./src/main/resources/static/logo.svg "a title")
This project provides an API for Scholexplorer, 
a scholarly communication platform that aggregates and analyzes scholarly information links from OpenAIRE Graphs. 
The API is built using Spring Boot and Swagger, and consists of two entry points defining two different
version of [Scholix Metadata format](https://www.scholix.org):

## Version 1
1. listDatasources: 
   - summary: Get all Datasources
   - description:  returns a list of all datasources
2. linksFromPublisher
   - summary: get all Scholix relation collected from a publisher
   - description:  return a list of scholix object published from a specific publisher
3. linksFromPid
   - summary:  Retrieve all scholix links from a persistent identifier
   - description: The linksFromPid endpoint returns a list of scholix object related from a specific persistent identifier
4. linksFromDatasource:
    - summary: Get all Scholix relation collected from a datasource
    - description: return a list of scholix object collected from a specific datasource
## Version 2
1. Links
   - summary:Get Scholix Links
   - description:
2. LinkPublisher/inTarget
   - summary:Get All Publishers that provide target object
   - description:Return a List of all Publishers that provide source objects in Scholix links and the total number of links where the target object comes from this publisher
3. LinkPublisher/inSource
   - summary:Get All Publishers that provide source object
   - description:Return a List of all Publishers that provide source objects in Scholix links and the total number of links where the source object comes from this publisher
4. LinkProvider
    - summary: Get all Link Providers
    - description: Return a list of link provider and relative number of relations

## Version 3

    